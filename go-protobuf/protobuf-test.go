package main

import (
	"github.com/golang/protobuf/proto"
	"log"
)

func main() {

	log.Println("1111")

	p := &Person{
		Id:    1234,
		Name:  "John Doe",
		Email: "jdoe@example.com",
		Phones: []*Person_PhoneNumber{
			{Number: "555-4321", Type: Person_HOME},
		},
	}

	marshal, _ := proto.Marshal(p)

	book := &Person{}
	if err := proto.Unmarshal(marshal, book); err != nil {
		log.Fatalln("Failed to parse address book:", err)
	}

	log.Println("==== ", book)

}
