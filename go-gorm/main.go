package main

import (
	"errors"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"time"
)

func main() {

	dsn := "root:123456@tcp(127.0.0.1:3306)/baby?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		return
	}
	//var user User
	user := new(User)
	db.First(&user)

	db.Take(&user)

	db.Last(&user)

	result := db.First(&user)
	affected := result.RowsAffected // 返回找到的记录数
	log.Println("count -", affected)
	err = result.Error

	errors.Is(result.Error, gorm.ErrRecordNotFound)

	log.Println("-----", user)

}

func (User) TableName() string {
	return "tb_user"
}

type User struct {
	Id         int32
	Name       string
	Password   string
	Status     int
	CreateTime time.Time
	UpdateTime time.Time
}
