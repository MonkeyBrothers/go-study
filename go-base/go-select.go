package main

func main() {
	defaultTest()
}

func defaultTest() {
	ch := make(chan int)
	select {
	case i := <-ch:
		println(i)
	default:
		println("default")
	}

}
