package main

import (
	"fmt"
	"time"
)

func main() {
	//parseUint, _ := strconv.ParseUint("-1239485039456392849", 10, 64)
	//log.Println(parseUint)

	//var mapList map[string]int
	//
	//log.Println("mapList ", mapList)
	//
	//var haha = make(map[string]int, 100)
	//
	//log.Println("jjj ", haha)
	//
	//mapList = map[string]int{"one": 1, "tow": 2}
	//
	//mapList["new"] = 1000
	//mapList["new1"] = 1000
	//mapList["new2"] = 1000
	//mapList["new3"] = 1000
	//mapList["new1"] = 2000
	//
	//for k, v := range mapList {
	//	log.Println("key:", k, "value:", v)
	//}
	//
	//var sceneList []string
	//
	//for k := range mapList {
	//	sceneList = append(sceneList, k)
	//}
	//
	//log.Println("sceneList:", sceneList)
	//
	//sort.Strings(sceneList)
	//
	//log.Println("sceneList:", sceneList)
	//
	//delete(mapList, "new3")
	//
	//for k, v := range mapList {
	//	log.Println("key:", k, "value:", v)
	//}

	pingTicker := time.NewTicker(time.Second * 1)

	ch1 := make(chan int)

	go func() {
		time.Sleep(time.Second)
		ch1 <- 1
	}()

	for {
		select {
		case i := <-ch1:
			fmt.Printf("从ch1读取了数据%d\n", i)
		case j := <-pingTicker.C:
			fmt.Printf("从ch2读取了数据%d\n", j)
		}
	}

}
