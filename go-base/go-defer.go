package main

import (
	"fmt"
	"time"
)

func main() {
	computeTime()
}

func computeTime() {
	startedAt := time.Now()
	//defer fmt.Println(time.Since(startedAt))
	defer func() { fmt.Println(time.Since(startedAt)) }()
	time.Sleep(time.Second)

}
