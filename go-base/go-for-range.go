package main

import "fmt"

func main() {
	//arr := []int{1, 2, 3}
	//var newArr []*int
	//for i, _ := range arr {
	//	newArr = append(newArr, &arr[i])
	//}
	//for _, v := range arr {
	//	newArr = append(newArr, &v)
	//}
	//for _, v := range newArr {
	//	fmt.Println(*v)
	//}
	copy()
	//clearArray()
	//printMap()
}
func copy() {
	arr := []int{1, 2, 3}
	var newArr []*int
	//for i, _ := range arr {
	//	newArr = append(newArr, &arr[i])
	//}
	// 会临时产生一个副本用于接受，所以&v都会指向最后的这个值
	for _, v := range arr {
		newArr = append(newArr, &v)
	}
	for _, v := range newArr {
		println(v)
		println(*v)
	}
}

// 清空
func clearArray() {
	arr := []int{1, 2, 3}
	fmt.Println("清除之前的数据：", arr)
	for i, _ := range arr {
		arr[i] = 0
	}
	fmt.Println("清除之后的数据：", arr)
}

// 随机打印
func printMap() {
	h := map[string]int{
		"1": 1,
		"2": 2,
		"3": 3,
		"4": 4,
	}

	for _, v := range h {
		println(v)
	}
}
