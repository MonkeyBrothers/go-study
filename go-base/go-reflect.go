package main

import (
	"fmt"
	"reflect"
)

func main() {
	//a := 100
	//reflectTest1(a)
	user := UserInfo{
		"houry",
		18,
	}
	reflectTest2(user)
}

func reflectTest2(s interface{}) {
	typeOf := reflect.TypeOf(s)
	fmt.Println("typeOf:", typeOf)
	fmt.Println("typeOf Kind:", typeOf.Kind())
	fmt.Println("typeOf NumField:", typeOf.NumField())
	valueOf := reflect.ValueOf(s)
	fmt.Println("valueOf", valueOf)
	iV := valueOf.Interface()
	fmt.Printf("iv=%v iv type=%T", iV, iV)
	fmt.Println()
	info, ok := iV.(UserInfo)
	if ok {
		fmt.Println("info.Name=", info.Name)
	}

}

type UserInfo struct {
	Name string
	Age  int8
}

func reflectTest1(b interface{}) {
	rType := reflect.TypeOf(b)
	fmt.Println("fType:", rType)

	rValue := reflect.ValueOf(b)
	fmt.Println("rValue:", rValue)
	n2 := rValue.Int() + 10
	fmt.Println("n2:", n2)
	fmt.Printf("rVal=%v rVal type=%T\n", rValue, rValue)

	IV := rValue.Interface()
	fmt.Println("Iv:", IV)
	v := IV.(int)
	fmt.Println("v:", v)
}
