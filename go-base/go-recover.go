package main

import "fmt"

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()
	test1()
	test2()
	test3()
}

func test1() {

	fmt.Println("test1")
}

func test2() {
	//defer func() {
	//	if err := recover(); err != nil {
	//		fmt.Println(err)
	//	}
	//}()
	fmt.Println("test2")
	panic("test 2 is panic")
}

func test3() {
	fmt.Println("test3")
}
