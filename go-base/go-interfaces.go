package main

import "fmt"

func main() {
	var animal Animal
	animal = &Dog{}
	fmt.Println(animal.Move())

	user := &User{Name: "tom"}
	var s Stringer = user
	fmt.Println(s.String())
}

type Stringer interface {
	String() string
}

type User struct {
	Name string
}

func (u User) String() string {
	return fmt.Sprintf("User: %s", u.Name)
}

type Animal interface {
	Move() string
}
type Pet interface {
	Name() string
}

// PetAnimal 接口组合
type PetAnimal interface {
	Animal
	Pet
}
type Dog struct {
}

//	func (d Dog) Move() string {
//		return "dog is moving"
//	}
func (d *Dog) Move() string {
	return "dog is moving"
}
