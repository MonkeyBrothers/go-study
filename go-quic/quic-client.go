package main

import (
	"context"
	"crypto/tls"
	"github.com/quic-go/quic-go"
	"io"
	"log"
	"time"
)

func main() {
	tlsConf := &tls.Config{
		InsecureSkipVerify: true,
		NextProtos:         []string{"barrage.houry.top"},
	}
	conn, err := quic.DialAddr("localhost:4242", tlsConf, nil)
	if err != nil {
		return
	}

	go func() {
		for {
			stream, err := conn.AcceptStream(context.Background())
			if err != nil {
				return
			}

			all, err := io.ReadAll(stream)
			log.Println("revc=", string(all))
			stream.Close()
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 1)
			stream, err2 := conn.OpenStreamSync(context.Background())
			if err2 != nil {
				return
			}
			stream.Write([]byte("22144"))
			stream.Close()
		}
	}()

	select {}
	//buf := make([]byte, len("hello"))
	//_, err = io.ReadFull(stream, buf)
	//if err != nil {
	//	return
	//}
	//fmt.Printf("Client: Got '%s'\n", buf)

}
