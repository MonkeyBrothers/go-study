package main

import (
	"context"
	"fmt"
	"github.com/quic-go/webtransport-go"
	"io"
	"log"
)

func main() {

	background := context.Background()
	var d webtransport.Dialer
	rsp, conn, err := d.Dial(background, "https://barrage.houry.top/test", nil)

	if err != nil {
		fmt.Println("err1=", err)
	}
	fmt.Println("rsp.state=", rsp.StatusCode)

	for {
		if err != nil {
			log.Println("client ee", err)
		} else {
			str, err := conn.AcceptStream(background)
			all, err := io.ReadAll(str)
			if err != nil {
				log.Println("client ee", err)
			}
			fmt.Println("data=", string(all))
			_ = str.Close()
		}
	}

}
