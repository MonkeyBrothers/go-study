package main

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
)

var globalMap = map[string]string{}

func main() {
	r := gin.Default()

	r.POST("/put", func(c *gin.Context) {
		param := DataInfo{}
		err := c.ShouldBindBodyWith(&param, binding.JSON)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"code": -2000, "msg": "error"})
			return
		}
		marshal, err := json.Marshal(param)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"code": -2000, "msg": "error"})
			return
		}
		globalMap["first"] = string(marshal)

		c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "ok"})
	})

	r.GET("/get", func(c *gin.Context) {

		param := DataInfo{}

		err := json.Unmarshal([]byte(globalMap["first"]), &param)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"code": -2000, "msg": "unmarshal error"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "ok", "data": param})
	})

	err := r.Run("localhost:8089")
	if err != nil {
		return
	}
}

type DataInfo struct {
	GPSLatitude  string `json:"GPSLatitude"`
	GPSLongitude string `json:"GPSLongitude"`
	GPSTime      string `json:"GPSTime"`
}
