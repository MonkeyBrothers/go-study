package main

import (
	"fmt"
	"net"
)

func main() {
	SocketClient()
}
func SocketClient() {
	conn, err := net.Dial("tcp", ":8080")
	if err != nil {
		fmt.Printf("连接服务端异常 %v\n", err)
		return
	}

	msg := "hello"

	n, err := conn.Write([]byte(msg))

	if err != nil {
		fmt.Printf("发送数据异常 %v\n", err)
		return
	}

	fmt.Printf("发送数据长度: %v\n", n)

	defer conn.Close()

}
