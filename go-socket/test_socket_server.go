package main

import (
	"fmt"
	"net"
)

func main() {
	SocketServer()
}

func SocketServer() {
	listen, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Printf("socket服务初始化失败: %v\n", err)
		return
	}
	for {
		fmt.Println("-------socket 服务启动成功 监听中---------")
		conn, err := listen.Accept()
		if err != nil {
			fmt.Printf("socket接收请求异常: %v\n", err)
			return
		}

		go socketHandler(conn)
	}
}

func socketHandler(conn net.Conn) {
	for {
		buf := make([]byte, 1024)

		count, err := conn.Read(buf)
		if err != nil {
			fmt.Printf("socket接收数据异常: %v\n", err)
			defer conn.Close()
			return
		}

		fmt.Printf("接收数据长度: %v\n", count)
		fmt.Printf("接收数据内容: %v\n", string(buf))
	}

}
