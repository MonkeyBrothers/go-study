package main

import (
	"bytes"
	"io"
	"log"
	"os"
)

func main() {
	//log.SetPrefix("mysel:")
	//log.SetFlags(log.Lshortfile | log.Ldate | log.Lmicroseconds)
	//log.Println("11111")
	////log.Panicln("出错误了")
	//log.Fatalln("程序即将退出")
	//log.Println("11111")

	//buf := &bytes.Buffer{}
	//
	//logger := log.New(buf, "barrage:", log.Lshortfile|log.LstdFlags)
	//
	//logger.Printf("%s login, age:%s", " u.Name", "u.Age")
	//
	//fmt.Print(buf.String())

	write1 := &bytes.Buffer{}

	write2 := os.Stdout

	getwd, _ := os.Getwd()
	s := getwd + "/go-log/log.txt"

	write3, err := os.OpenFile(s, os.O_WRONLY|os.O_CREATE, 0755)

	if err != nil {
		log.Fatalln("create file log.txt failed:", err)
	}

	logger := log.New(io.MultiWriter(write1, write2, write3), "", log.Lshortfile|log.LstdFlags)

	logger.Println("ceshixinxi")
}
