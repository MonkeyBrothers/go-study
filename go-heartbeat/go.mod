module heartbeat

go 1.18

require (
	github.com/gomodule/redigo v1.8.9 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
)
