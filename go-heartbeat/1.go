package main

//
//import (
//	"context"
//	"encoding/json"
//	"errors"
//	"github.com/gomodule/redigo/redis"
//	"log"
//	"sync"
//	"time"
//)
//
//var client redis.Conn
//
//const unlockScript = `
//if redis.call("get",KEYS[1]) == ARGV[1] then
//    return redis.call("del",KEYS[1])
//else
//    return 0
//end`
//
//func main() {
//	//var counter = struct {
//	//	sync.RWMutex
//	//	m map[string]int
//	//}{m: make(map[string]int)}
//	//
//	//eventMaps := RWMap{
//	//	m: make(map[string]interface{}),
//	//}
//	//
//	//eventMaps.Set("aaa", Event{
//	//	Id: "12345",
//	//})
//	//
//	//get, b := eventMaps.Get("aaa")
//	//if get != nil {
//	//
//	//}
//	//log.Println("---", get, b)
//	//
//	//// TODO 接收到心跳事件之后创建 心跳事件
//
//}
//
//func lottery(ctx context.Context) error {
//	// 加锁
//	myRandomValue := gofakeit.UUID()
//	resourceName := "resource_name"
//	ok, err := client.SetNX(ctx, resourceName, myRandomValue, time.Second*30).Result()
//	if err != nil {
//		return err
//	}
//	if !ok {
//		return errors.New("系统繁忙，请重试")
//	}
//	// 解锁
//	defer func() {
//		script := redis.NewScript(unlockScript)
//		script.Run(ctx, client, []string{resourceName}, myRandomValue)
//	}()
//
//	// 业务处理
//	time.Sleep(time.Second)
//	return nil
//}
//
//func (m *RWMap) Delete(k int) { //删除一个键
//	m.Lock() // 锁保护
//	defer m.Unlock()
//	//delete(m.m, k)
//}
//
//func (m *RWMap) Len() int { // map的长度
//	m.RLock() // 锁保护
//	defer m.RUnlock()
//	return len(m.m)
//}
//
//func (m *RWMap) Each(f func(k, v int) bool) { // 遍历map
//	m.RLock() //遍历期间一直持有读锁
//	defer m.RUnlock()
//
//	//for k, v := range m.m {
//	//if !f(k, v) {
//	//	return
//	//}
//	//}
//}
//
////func AddEvent(event string, eventMap *RWMap) bool {
////
////ev, flag := NewEvent(event)
////if !flag || ev.Count == 0 {
////	return false
////}
////events, flag := eventMap.Get(ev.Id)
////
////e := events.(Event)
////
////// 放入Map中
////return true
//
////}
//
//func (m *RWMap) Get(k string) (interface{}, bool) {
//	m.RLock()
//	defer m.RUnlock()
//	v, existed := m.m[k]
//	return v, existed
//}
//
//func (m *RWMap) Set(k string, v interface{}) { // 设置一个键值对
//	m.Lock() // 锁保护
//	defer m.Unlock()
//	m.m[k] = v
//}
//
//func NewEvent(e string) (Event, bool) {
//	err := json.Unmarshal([]byte(e), &Event{})
//	if err != nil {
//		log.Fatalln("解析心跳事件异常:", err)
//		return Event{}, false
//	}
//	return Event{}, true
//}
//
//type RWMap struct {
//	sync.RWMutex
//	m map[string]interface{}
//}
//
//type Event struct {
//	Id           string
//	Type         string
//	Count        int32
//	IntervalTime int32
//	CreateTime   time.Time
//}
